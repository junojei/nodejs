exports.user = {
    id: {fillRule: null},
    firstName: {fillRule: /^\S+$/},
    lastName: {fillRule: /^\S+$/},
    email: {fillRule: /^[a-zA-Z]{1}\S+\@(gmail\.com)$/},
    phoneNumber: {fillRule: /^(\+380){1}(\d{9})$/},
    password: {fillRule: /\S{3,}$/} // min 3 symbols
}