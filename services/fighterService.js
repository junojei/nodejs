const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    all() {
        const items = FighterRepository.getAll();
        if (!items) {
            throw {
                status: 404,
                message: 'Database empty'
            };
        }
        return items;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            throw {
                status: 404,
                message: 'Fighter not found'
            };
        }
        return item;
    }

    create(data) {
        if (FighterRepository.getOne({ name: data.name })) {
            throw {
                status: 400,
                message: 'This name already use'
            };
        }

        const item = FighterRepository.create(data);
        if (!item) {
            throw {
                status: 400,
                message: 'Fighter not create'
            };
        }
        return item;
    }

    update(id, dataToUpdate) {
        if (!FighterRepository.getOne({ id: id })) {
            throw {
                status: 400,
                message: 'Fighter not find by this ID'
            };
        }

        if (FighterRepository.getOne({ name: data.name })) {
            throw {
                status: 400,
                message: 'This name already use'
            };
        }

        const item = FighterRepository.update(id, dataToUpdate);
        if (!item) {
            throw {
                status: 400,
                message: 'Fighter not update'
            };
        }
        return item;
    }

    delete(id) {
        if (!FighterRepository.getOne({ id: id })) {
            throw {
                status: 400,
                message: 'Fighter not find by this ID'
            };
        }

        const item = FighterRepository.delete(id);
        if (!item) {
            throw {
                status: 400,
                message: 'Fighter not delete'
            };
        }
        return item;
    }
}

module.exports = new FighterService();