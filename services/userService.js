const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    all() {
        const items = UserRepository.getAll();
        if (!items) {
            throw {
                status: 404,
                message: 'Database empty'
            };
        }
        return items;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            throw {
                status: 404,
                message: 'User not found'
            };
        }
        return item;
    }

    create(data) {
        if (UserRepository.getOne({ email: data.email })) {
            throw {
                status: 400,
                message: 'This email already use'
            };
        }

        if (UserRepository.getOne({ phoneNumber: data.phoneNumber })) {
            throw {
                status: 400,
                message: 'This phone already use'
            };
        }

        const item = UserRepository.create(data);
        if (!item) {
            throw {
                status: 400,
                message: 'User not create'
            };
        }
        return item;
    }

    update(id, dataToUpdate) {
        if (!UserRepository.getOne({ id: id })){
            throw {
                status: 400,
                message: 'User not find by this ID'
            };
        }

        if (UserRepository.getOne({ email: data.email })) {
            throw {
                status: 400,
                message: 'This email already use'
            };
        }

        if (UserRepository.getOne({ phoneNumber: data.phoneNumber })) {
            throw {
                status: 400,
                message: 'This phone already use'
            };
        }

        const item = UserRepository.update(id, dataToUpdate);
        if (!item) {
            throw {
                status: 400,
                message: 'User not update'
            };
        }
        return item;
    }

    delete(id) {
        if (!UserRepository.getOne({ id: id })){
            throw {
                status: 400,
                message: 'User not find by this ID'
            };
        }

        const item = UserRepository.delete(id);
        if (!item) {
            throw {
                status: 400,
                message: 'User not delete'
            };
        }
        return item;
    }
}

module.exports = new UserService();