const { fighter } = require('../models/fighter');
const {
    checkBodyWithOutId,
    checkOfExcessiveProperties,
    checkAllfieldsWithOutOne,
    checkHasMoreOneProperties,
} = require('../services/validService');

function checkFormatData(data) {
    if (!data.name) {
        throw { status: 400, message: 'Empty name' };
    }
    if (!data.health) {
        data.health = "100";
    }
    if (!data.power) {
        throw { status: 400, message: 'Empty power' };
    }
    if (!data.defense) {
        throw { status: 400, message: 'Empty defense' };
    }
    checkHealth(data.health);
    checkPower(data.power);
    checkDefense(data.defense);
}

function checkFormatDataForUpdate(data) {
    if (data.name === '') {
        throw { status: 400, message: 'Empty name' };
    }
    if (data.health === '') {
        throw { status: 400, message: 'Empty health' };
    }
    if (data.power === '') {
        throw { status: 400, message: 'Empty power' };
    }
    if (data.defense === '') {
        throw { status: 400, message: 'Empty defense' };
    }
    if (data.health) {
        checkHealth(data.health);
    }
    if (data.power) {
        checkPower(data.power);
    }
    if (data.defense) {
        checkDefense(data.defense);
    }
}

function isNumber(value) {
    let regExp = /^[0-9]+$/g;
    return regExp.test(value);
}

function checkHealth(health) {
    if (!isNumber(health)) {
        throw { status: 400, message: 'Health is not a number' };
    } else {
        const value = parseInt(health);
        if (!(value > 80 && value < 120)) {
            throw { status: 400, message: 'Health is not in range (80, 120)' };
        }
    }
}

function checkPower(power) {
    if (!isNumber(power)) {
        throw { status: 400, message: 'Power is not a number' };
    } else {
        const value = parseInt(power);
        if (!(value > 1 && value < 100)) {
            throw { status: 400, message: 'Power is not in range (1, 100)' };
        }
    }
}

function checkDefense(defense) {
    if (!isNumber(defense)) {
        throw { status: 400, message: 'Defense is not a number' };
    } else {
        const value = parseInt(defense);
        if (!(value > 1 && value < 10)) {
            throw { status: 400, message: 'Defense is not in range (1, 10)' };
        }
    }
}

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation

    try {
        const fighterData = req.body;
        const { id, ...otherProperties } = fighter;

        if (checkBodyWithOutId(fighterData)) {
            throw { status: 400, message: 'Request must be without id' };
        }
        if (!checkOfExcessiveProperties(otherProperties, fighterData)) {
            throw { status: 400, message: 'Request has unnecessary fields' };
        }
        if (checkAllfieldsWithOutOne(otherProperties, fighterData, 'health')) {
            throw { status: 400, message: 'Request has not required fields' };
        }

        checkFormatData(fighterData);

        next();
    } catch (err) {
        res.status(err.status).json({
            error: true,
            message: err.message,
        });
    }
};

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update

    try {
        const paramId = req.params.id;
        const fighterData = req.body;
        const { id, ...otherProperties } = fighter;

        if (!paramId) {
            throw { status: 400, message: 'There is no id in request' };
        }
        if (checkBodyWithOutId(fighterData)) {
            throw { status: 400, message: 'Request must be without id' };
        }
        if (!checkOfExcessiveProperties(otherProperties, fighterData)) {
            throw { status: 400, message: 'Request has unnecessary fields' };
        }
        if (!checkHasMoreOneProperties(otherProperties, fighterData)) {
            throw { status: 400, message: 'Request has not necessary fields' };
        }

        checkFormatDataForUpdate(fighterData);

        next();
    } catch (err) {
        res.status(err.status).send({
            error: true,
            message: err.message,
        });
    }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;