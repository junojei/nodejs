const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query

    if (res.err) {
        res.status(res.err.status).send({
            error: true,
            message: res.err.message
        })
    }

    res.status(200).send(res.data);

    next();
}

exports.responseMiddleware = responseMiddleware;