const { user } = require('../models/user');
const {
    checkBodyWithOutId,
    checkOfExcessiveProperties,
    checkAllfields,
    checkHasMoreOneProperties,
} = require('../services/validService');

function checkFormatData(data) {
    if (!data.firstName) {
        throw { status: 400, message: 'Empty firstName' };
    }
    if (!data.lastName) {
        throw { status: 400, message: 'Empty lastName' };
    }
    if (!data.email) {
        throw { status: 400, message: 'Empty email' };
    }
    if (!data.phoneNumber) {
        throw { status: 400, message: 'Empty phone number' };
    }
    if (!data.password) {
        throw { status: 400, message: 'Empty password' };
    }
    if (!checkEmail(data.email)) {
        throw { status: 400, message: 'Invalid email' };
    }
    if (!checkPhoneNumber(data.phoneNumber)) {
        throw { status: 400, message: 'Invalid phone number' };
    }
    if (!checkPassword(data.password)) {
        throw { status: 400, message: 'Invalid password' };
    }
}

function checkFormatDataForUpdate(data) {
    if (data.firstName === '') {
        throw { status: 400, message: 'Empty firstname' };
    }
    if (data.lastName === '') {
        throw { status: 400, message: 'Empty lastname' };
    }
    if (data.email === '') {
        throw { status: 400, message: 'Empty email' };
    }
    if (data.phoneNumber === '') {
        throw { status: 400, message: 'Empty phone number' };
    }
    if (data.password === '') {
        throw { status: 400, message: 'Empty password' };
    }
    if (data.email) {
        if (!checkEmail(data.email)) {
            throw { status: 400, message: 'Invalid email' };
        }
    }
    if (data.phoneNumber) {
        if (!checkPhoneNumber(data.phoneNumber)) {
            throw { status: 400, message: 'Invalid phone number' };
        }
    }
    if (data.password) {
        if (!checkPassword(data.password)) {
            throw { status: 400, message: 'Invalid password' };
        }
    }
}

function checkEmail(email) {
    let regExp = user.email.fillRule;
    return regExp.test(email);
}

function checkPhoneNumber(phoneNumber) {
    let regExp = user.phoneNumber.fillRule;
    return regExp.test(phoneNumber);
}

function checkPassword(password) {
    let regExp = user.password.fillRule;
    return regExp.test(password);
}

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation

    try {
        const userData = req.body;
        const { id, ...otherProperties } = user;

        if (checkBodyWithOutId(userData)) {
            throw { status: 400, message: 'Request must be without id' };
        }
        if (!checkOfExcessiveProperties(otherProperties, userData)) {
            throw { status: 400, message: 'Request has unnecessary fields' };
        }
        if (checkAllfields(otherProperties, userData)) {
            throw { status: 400, message: 'Request has not required fields' };
        }
        checkFormatData(userData);

        next();
    } catch (err) {
        res.status(err.status).send({
            error: true,
            message: err.message
        });
    }
};

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        const paramId = req.params.id;
        const userData = req.body;
        const { id, ...otherProperties } = user;

        if (!paramId) {
            throw { status: 400, message: 'There is no id in request' };
        }
        if (checkBodyWithOutId(userData)) {
            throw { status: 400, message: 'Request must be without id' };
        }
        if (!checkOfExcessiveProperties(otherProperties, userData)) {
            throw { status: 400, message: 'Request has unnecessary fields' };
        }
        if (!checkHasMoreOneProperties(otherProperties, userData)) {
            throw { status: 400, message: 'Request has not necessary fields' };
        }

        checkFormatDataForUpdate(userData);

        next();
    } catch (err) {
        res.status(err.status).send({
            error: true,
            message: err.message
        });
    }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;